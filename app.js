const express = require("express");
const bodyParser = require("body-parser");
const port = process.env.PORT || 3000;
const cors = require("cors");
const socketIo = require("socket.io");
const morgan = require("morgan");
const multer = require("multer");
const path = require("path");
const { Usuarios } = require("./classes/usuario");
const { crearMensaje } = require("./utilidades/utilidades");
const { v4: uuidv4 } = require("uuid");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const conn = require("./connection");
const app = express();
const methodOverride = require("method-override");
const socketIO = require("socket.io");
const http = require("http");

let server = http.createServer(app);
let io = socketIO(server);

var Fake = [
  "Hi there, I'm Jesse and you?",
  "Nice to meet you",
  "How are you?",
  "Not too bad, thanks",
  "What do you do?",
  "That's awesome",
  "Codepen is a nice place to stay",
  "I think you're a nice person",
  "Why do you think that?",
  "Can you explain?",
  "Anyway I've gotta go now",
  "It was a pleasure chat with you",
  "Time to make a new codepen",
  "Bye",
  ":)",
];

io.on("connection", (client) => {
  client.on("chat:room", (data) => {
    console.log(client.id);
    client.emit("chat:room", client.id);
  });

  client.on("chat:message", (data) => {
    console.log(data);
    client.emit("chat:message", { message: Fake[data.index] });
  });
});

//webpack
const webpack = require("webpack");
const webpackDevMiddleware = require("webpack-dev-middleware");
const config = require("./webpack.config");

const usuarios = new Usuarios();

//--------------STATIC FILES--------------
//app.use(express.static(__dirname + "/public"));
app.use("/public", express.static("public"));
//--------------JSON--------------
app.set("view engine", "pug");
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(methodOverride("_method"));

//midlleware-------------------------------------------
app.use(webpackDevMiddleware(webpack(config)));
app.use(express.static(path.join(__dirname, "public")));

//-----------------sockets-----------------------------
io.on("connection", (client) => {
  console.log("socket connected", client.id);
  client.on("crearMensaje", (data) => {
    let persona = usuarios.getPersona(client.id);
    let mensaje = crearMensaje(persona.nombre, data.mensaje);
    client.broadcast.emit("crearMensaje", mensaje);
  });

  client.on("mensajePrivado", (data) => {
    let persona = usuarios.getPersona(client.id);

    client.broadcast
      .to(data.para)
      .emit("mensajePrivado", crearMensaje(persona.nombre, data.mensaje));
  });

  client.on("entrarChat", (data, callback) => {
    if (!data.nombre || !data.sala) {
      return callback({
        error: true,
        mensaje: "el nombre es necesario",
      });
    }

    client.join(data.sala);
    usuarios.agregarPersona(client.id, data.nombre, data.sala);

    client.broadcast
      .to(data.sala)
      .emit("listarPersonas", usuarios.getPersonasPorSala(data.sala));

    callback(usuarios.getPersonasPorSala(data.sala));
  });
});

//--------------MIDDLEWARE MORGAN--------------
app.use(morgan("dev"));
const storage = multer.diskStorage({
  destination: "public/assets/images",
  filename: (req, file, cb) => {
    cb(null, uuidv4() + path.extname(file.originalname).toLocaleLowerCase());
  },
});

app.use(
  multer({
    storage,
  }).single("image")
);
//--------------MIDDLEWARE SESSIONS--------------
app.use(
  session({
    secret: process.env.SESSION_SECRET || "some-secret",
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({
      mongooseConnection: conn,
    }),
  })
);

//--------------ROUTES--------------
const news = require("./routes/news.route");
const users = require("./routes/user.route");
const main = require("./routes/main.route");
const comments = require("./routes/comment.route");
app.use("/news", news);
app.use("/users", users);
app.use("/comments", comments);
app.use("/", main);

//--------------TEST------------------
//app.get("/", (req, res) => {
//  req.session.cuenta = req.session.cuenta ? req.session.cuenta + 1 : 1;
//  res.status(200).send(`Hola has visto esta página ${req.session.cuenta}`);
//});
//--------------LISTEN PORT--------------
server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
