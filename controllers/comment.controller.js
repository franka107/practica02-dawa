const Comment = require("../models/comment.models");


exports.comment_list = function (req, res) {
  Comment.find({}, (err, comments) => {
    res.send(comments);
  });
};
exports.comment_byid= function (req, res) {
  Comment.find({post_id: req.params.id}, (err, comments) => {
    res.send(comments);
  });
};

exports.comment_create = function (req, res) {
  let post = req.body.post_id
  let comment = new Comment({
    user_id: req.body.user_id,
    post_id: req.body.post_id,
    content: req.body.content,
    date_time: Date.now(),
  });
  comment.save((err) => (err ? next(err) : res.redirect(`/news/${post}`)));
};

exports.comment_delete = function (req, res) {
  Comment.findByIdAndRemove(req.params.id, (err) => {
    if (err) return next(err);
    res.send("Eliminado exitosamente");
  });
};

