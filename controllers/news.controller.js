const New = require("../models/news.models");
const Comment = require("../models/comment.models");
const User = require("../models/user.models");

exports.news_list = function (req, res) {
  let news = [];
  let user = req.session.user;
  let properties = {};
  let aux = [];
  New.find({}).then(function (publications) {
    if (publications) news = publications;
    res.render("index", { news, user });
  });
};

exports.news_renderupdate = function (req, res, next) {
  let user = req.session.user;
  New.findById(req.params.id, (err, singlenew) => {
    if (err) return next(err);
    res.render("edit", { singlenew, user, completed: req.query.completed });
  });
};
exports.product_create = function (req, res) {
  let product = new New({
    title: req.body.title,
    description: req.body.description,
    short_description: req.body.short_description,
    image: "public/assets/images/" + req.file.filename,
    user_id: req.body.id,
  });
  product.save((err) => (err ? next(err) : res.send("Publicacion creada")));
};
exports.product_details = function (req, res, next) {
  New.findById(req.params.id, (err, product) => {
    if (err) return next(err);
    res.send(product);
  });
};
exports.product_delete = function (req, res, next) {
  let user = req.session.user;

  New.findByIdAndRemove(req.params.id, (err) => {
    if (err) return next(err);
    res.redirect("/");
  });
};
exports.news_update = function (req, res) {
  let fields = {
    title: req.body.title,
    description: req.body.description,
    short_description: req.body.short_description,
  };
  if (req.file) {
    fields.image = "public/assets/images/" + req.file.filename;
  }
  New.findByIdAndUpdate(req.params.id, { $set: fields }, (err, singlenew) => {
    if (err) return next(err);
    res.redirect(`/news/${req.params.id}/update?completed=true`);
  });
};

exports.news_details = async function (req, res, next) {
  let comments = [];
  await Comment.find({ post_id: req.params.id }, (err, listcomments) => {
    if (listcomments) comments = listcomments;
    console.log(comments);
  });
  let user = req.session.user;
  await New.findById(req.params.id, (err, product) => {
    if (err) return next(err);
    res.render("details", { product, user, comments });
  });
};

exports.news_create = function (req, res) {
  let user = req.session.user;
  res.render("create", { user });
};

exports.news_sendcreate = function (req, res, next) {
  let singlenew = new New({
    title: req.body.title,
    description: req.body.description,
    short_description: req.body.short_description,
    image: "public/assets/images/" + req.file.filename,
    user_id: req.session.user._id,
  });
  singlenew.save((err) => (err ? next(err) : res.redirect("/")));
};
