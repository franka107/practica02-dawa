const User = require("../models/user.models");
const bcrypt = require("bcrypt");
const querystring = require("querystring");

exports.user_showlogin = function (req, res) {
  res.render("login", {
    valid: req.query.valid,
  });
};
exports.user_verifylogin = async function (req, res, next) {
  let isLoginSuccessfully;
  await User.findOne(
    {
      email: req.body.email,
    },
    function (err, user) {
      if (user) {
        isLoginSuccessfully = bcrypt.compareSync(
          req.body.password,
          user.password
        );
        if (isLoginSuccessfully) {
          req.session.user = user;
          res.redirect("/");
        } else {
          res.render("login", { error: true });
        }
      } else {
        res.render("login", { error: true });
      }
    }
  );
};

exports.user_sendregister = function (req, res, next) {
  let user = new User({
    full_name: req.body.full_name,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 10),
  });
  user.save((err) =>
    err ? next(err) : res.redirect("/users/login?valid=true")
  );
};

exports.user_logout = function (req, res, next) {
  req.session.destroy();
  res.redirect("/");
};

exports.user_createuser = function (req, res) {
  let user = new User({
    full_name: "Georgiiituuuu Puma Salcedo",
    email: "george.puma@tecsup.edu.pe",
    password: bcrypt.hashSync("123456", 10),
  });
  user.save((err) => (err ? console.log(err) : res.send("Usuario creado")));
};

//exports.login = function (req, res) {
//  res.render('login')
//};
//
exports.register = function (req, res) {
  res.render("register");
};
