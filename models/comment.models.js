const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let CommentSchema = new Schema({
  user_id: {
    type: String,
    required: [true, "el ID del creador del comentario es necesario"],
  },
  post_id:{
    type: String,
    required: [true, "El Id del Post es necesario"]
  },
  content:{
    type: String,
    required: [true, "Contenido del comentario es necesario"]
  },
  date_time: {
    type: Date,
    required: [true, "Fecha del comentario necesario"],
  },

  
});

module.exports = mongoose.model("Comment", CommentSchema);
