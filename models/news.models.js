const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const moment = require("moment");

let NewSchema = new Schema({
  title: {
    type: String,
    required: [true, "El titulo de la noticia  es requerido"],
    max: 100,
  },
  description: {
    type: String,
    required: [true, "Descripcion de la noticia necesario"],
  },
  short_description: {
    type: String,
    required: [true, "Pequeña Descripcion de la noticia necesario"],
  },
  image: {
    type: String,
    required: [true, "Ubicacion del archivo"],
  },
  user_id: {
    type: String,
    require: [true, "ID del creado de la noticia"],
  },
  date_time: {
    type: Date,
    default: Date.now(),
    required: [true, "Fecha de la publicacion necesario"],
  },
  format_date: {
    type: String,
    default: moment().format("L"),
    required: [true, "Fecha de la publicacion necesario"],
  },
});

module.exports = mongoose.model("New", NewSchema);
