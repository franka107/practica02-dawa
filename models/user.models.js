const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let typesValid = {
  values: ["ADMIN", "CLIENT"],
  message: "{VALUE} no es un tipo valido",
};

let UserSchema = new Schema({
  full_name: {
    type: String,
    max: 100,
    required: [true, "El nombre del usuario es necesario"],
  },
  email: {
    type: String,
    max: 150,
    unique: true,
    required: [true, "El email del usuario es necesario"],
  },
  password: {
    type: String,
    require: [true, "El password del usuario es necesario"],
  },
  role: {
    type: String,
    default: "CLIENT",
    enum: typesValid,
  },
});

module.exports = mongoose.model("User", UserSchema);
