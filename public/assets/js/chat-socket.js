console.log('CONSOLE SOCKET');
var socket = io();
let idClient;

let messageSubmit = document.getElementById('message-submit');
let messageInput = document.getElementById('message-input');
let messageContent = document.getElementById('messages-content');

//Conectarse a socketIO
socket.on('connect', function () {
  console.log('Conectado al servidor');

  socket.emit('chat:room', function (resp) {
    console.log('Users online', resp);
  });
});

// Escuchar información
socket.on('chat:room', function (data) {
  console.log('Server:', data);
  idClient = data;
});

/* messageSubmit.addEventListener('click', function () {
  console.log('ACCION BUTTOn');
  messageContent.innerHTML += `<div class="message message-personal"> ${messageInput.value}</div>`;
});
 */

var $messages = $('#messages-content'),
  d,
  h,
  m,
  i = 0;

var cont = 0;

$('#message-submit').click(function () {
  console.log('ID BUTTON SUBMIT');
  insertMessage();
});

function insertMessage() {
  let msg = $('#message-input').val();
  if ($.trim(msg) == '') {
    return false;
  }
  console.log(msg);
  $('<div class="message message-personal">' + msg + '</div>')
    // .appendTo($('mCSB_container'))
    .appendTo($('#messages-content'))
    .addClass('new');
  setDate();
  $('#message-input').val(null);
  /* updateScrollbar();
  setTimeout(function () {
    fakeMessage();
  }, 1000 + Math.random() * 20 * 100); */
  let message = {user: idClient, message: msg, index: cont};
  socket.emit('chat:message', message);
  cont = cont + 1;
}

socket.on('chat:message', function (data) {
  console.log('MENSAJE RECIBIDO', data);

  $(
    '<div class="message loading new"><figure class="avatar"><img src="http://askavenue.com/img/17.jpg" /></figure><span></span></div>'
    // ).appendTo($('.mCSB_container'));
  ).appendTo($('#messages-content'));
  updateScrollbar();

  setTimeout(function () {
    $('.message.loading').remove();
    $(
      '<div class="message new"><figure class="avatar"><img src="http://askavenue.com/img/17.jpg" /></figure>' +
        data.message +
        '</div>'
    )
      // .appendTo($('.mCSB_container'))
      .appendTo($('#messages-content'))
      .addClass('new');
    setDate();
    updateScrollbar();
    i++;
  }, 1000 + Math.random() * 20 * 100);
});

function updateScrollbar() {
  /* $messages.mCustomScrollbar('update').mCustomScrollbar('scrollTo', 'bottom', {
    scrollInertia: 10,
    timeout: 0,
  }); */
}

function setDate() {
  d = new Date();
  if (m != d.getMinutes()) {
    m = d.getMinutes();
    $('<div class="timestamp">' + d.getHours() + ':' + m + '</div>').appendTo(
      $('.message:last')
    );
    $('<div class="checkmark-sent-delivered">&check;</div>').appendTo(
      $('.message:last')
    );
    $('<div class="checkmark-read">&check;</div>').appendTo($('.message:last'));
  }
}
