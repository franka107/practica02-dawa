const express = require("express");
const router = express.Router();

const comment_controller = require("../controllers/comment.controller");

router.get("/", comment_controller.comment_list);
router.get("/:id", comment_controller.comment_byid);
router.post("/create", comment_controller.comment_create);
router.delete("/:id/delete", comment_controller.comment_delete);


module.exports = router;
