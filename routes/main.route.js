const express = require("express");
const router = express.Router();

const news_controller = require("../controllers/news.controller");
const chat_controller = require("../controllers/chat.controller");

router.get("/", news_controller.news_list);

router.get("/chat", chat_controller.general_chat);

module.exports = router;
