const express = require("express");
const router = express.Router();

const news_controller = require("../controllers/news.controller");

//router.get("/", news_controller.product_list);
//router.post("/create/:id", news_controller.product_create);
//router.get("/:id", news_controller.product_details);
router.put("/:id/update", news_controller.news_update);
router.get("/:id/update", news_controller.news_renderupdate);
router.post("/:id/delete", news_controller.product_delete);
//router.get("/details", news_controller.news_details);
router.get("/create", news_controller.news_create);
router.post("/create", news_controller.news_sendcreate);
router.delete("/:id/delete", news_controller.product_delete);
router.get("/:id", news_controller.news_details);
//router.post('/create', product_controller.product_create)
//router.get('/:id', product_controller.product_details)
//router.put('/:id/update', product_controller.product_update)
//router.delete('/:id/delete', product_controller.product_delete)

module.exports = router;
