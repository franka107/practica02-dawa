const express = require("express");
const router = express.Router();

const user_controller = require("../controllers/user.controller");

router.get("/login", user_controller.user_showlogin);
router.post("/login", user_controller.user_verifylogin);
router.get("/createuser", user_controller.user_createuser);
router.get("/logout", user_controller.user_logout);
router.post("/register", user_controller.user_sendregister);
router.get("/register", user_controller.register);
//router.get("/test", news_controller.news_test);
//router.post('/create', product_controller.product_create)
//router.get('/:id', product_controller.product_details)
//router.put('/:id/update', product_controller.product_update)
//router.delete('/:id/delete', product_controller.product_delete)

module.exports = router;
